﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Sample_RPG_Framework
{
    public partial class Form1 : Form
    {

        int gridX = 9;
        int gridY = 9;
        string filepath = Application.StartupPath + "\\maptest.txt";
        PictureBox[,] mapGrid = new PictureBox[10, 10];
        MapStuff[,] mapStuff = new MapStuff[10, 10];
        int dudeX = 0;
        int dudeY = 0;
        Color dudeColor = Color.RoyalBlue;


        public Form1()
        {
            InitializeComponent();
            this.KeyDown += Form1_KeyDown;
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            mapGrid[dudeX, dudeY].BackColor = getMapColor(mapStuff[dudeX, dudeY].getColor());

            switch (e.KeyCode)
            {
                case Keys.W:

                    if (dudeY > 0)
                    {
                        if (mapStuff[dudeX, dudeY - 1].getCanPass())
                        {
                            dudeY--;
                        }
                    }
                    break;

                case Keys.S:

                    if (dudeY < gridY)
                    {
                        if (mapStuff[dudeX, dudeY + 1].getCanPass())
                        {
                            dudeY++;
                        }
                    }
                    break;

                case Keys.A:

                    if (dudeX > 0)
                    {
                        if (mapStuff[dudeX - 1, dudeY].getCanPass())
                        {
                            dudeX--;
                        }
                    }
                    break;

                case Keys.D:

                    if (dudeX < gridX)
                    {
                        if (mapStuff[dudeX + 1, dudeY].getCanPass())
                        {
                            dudeX++;
                        }
                    }
                    break;

                default:
                    break;
            }

            mapGrid[dudeX, dudeY].BackColor = dudeColor;

        }

        

        public struct MapStuff
        {
            int x { get; set; }
            int y { get; set; }
            string terrain { get; set; }
            public bool canpass { get; set; }
            string item { get; set; }

            public void setX(string x)
            {
                this.x = int.Parse(x);
            }
            public void setY(string y)
            {
                this.y = int.Parse(y);
            }
            public string getColor()
            {
                return this.terrain;
            }
            public void setCanPass(string canPass)
            {
                this.canpass = bool.Parse(canPass);
            }
            public void setItem(string item)
            {
                this.item = item;
            }
            public void setTerrain(string terrain)
            {
                this.terrain = terrain;
            }
            public bool getCanPass()
            {
                return this.canpass;
            }
        }

        private void createMap()
        {
            for (int x = 0; x <= gridX; x++)
            {
                for (int y = 0; y <= gridY; y++)
                {
                    PictureBox pic = new PictureBox();
                    pic.Left = 30 * x + 10;
                    pic.Top = 30 * y + 10;
                    pic.Height = 29;
                    pic.Width = 29;
                    mapGrid[x, y] = pic;
                    pic.BackColor = Color.Plum;
                    this.Controls.Add(pic);
                }
            }
        }

        private void draw_map()
        {
            for(int x = 0; x<= gridX; x++)
            {
                for (int y = 0;y <= gridY; y++)
                {
                    mapGrid[x, y].BackColor = getMapColor(mapStuff[x, y].getColor());
                }
            }
        }

        private Color getMapColor(string mapTerrain)
        {
            if (mapTerrain == "lava")
            {
                return Color.Red;
            }
            else if(mapTerrain == "grass")
            {
                return Color.LawnGreen;
            }
            else if (mapTerrain == "water")
            {
                return Color.Aquamarine;
            }
            else if (mapTerrain == "mountain")
            {
                return Color.DarkSlateGray;
            }
            else
            {
                return Color.Fuchsia;
            }
        }

        private void loadMap()
        {
            StreamReader obj = new StreamReader(filepath);
            string lineIn = "";
            string[] tmp;

            while(obj.Peek() != -1)
            {
                lineIn = obj.ReadLine();
                tmp = lineIn.Split('|');
                mapStuff[int.Parse(tmp[0]), int.Parse(tmp[1])].setX(tmp[0]);
                mapStuff[int.Parse(tmp[0]), int.Parse(tmp[1])].setY(tmp[1]);
                mapStuff[int.Parse(tmp[0]), int.Parse(tmp[1])].setTerrain(tmp[2]);
                mapStuff[int.Parse(tmp[0]), int.Parse(tmp[1])].setCanPass(tmp[3]);
                mapStuff[int.Parse(tmp[0]), int.Parse(tmp[1])].setItem(tmp[4]);
            }

            obj.Close();

        }
        

        private void Form1_Load(object sender, EventArgs e)
        {
            createMap();
            loadMap();
            draw_map();

            mapGrid[dudeX, dudeY].BackColor = dudeColor;
        }
    }
}
